from rest_framework import serializers
from timetable.models import Homework, Lesson


class HomeworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Homework
        fields = [
            'text',
            'author'
        ]


class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = [
            'homeworks',
            'name',
            'priority'
        ]
