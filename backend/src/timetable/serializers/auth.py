from rest_framework import serializers
from rest_framework.authtoken.models import Token

from timetable.models import User
from timetable.serializers.user import UserSerializer


class UserAuthSerializer(UserSerializer):
    auth_token = serializers.SerializerMethodField()

    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ("auth_token",)

    def get_auth_token(self, obj):
        token, created = Token.objects.get_or_create(user=obj)
        return token.key


class UserSignUpSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "group_number",
            "email",
            "password",
        ]


class UserAuthSignInSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("email", "password")
