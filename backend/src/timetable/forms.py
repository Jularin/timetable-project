from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from .models import User


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ["email", "group_number", "first_name", "last_name"]

        widgets = {
            "email": forms.TextInput(attrs={"class": "form-control"}),
            "group_number": forms.TextInput(attrs={"class": "form-control"}),
            "first_name": forms.TextInput(attrs={"class": "form-control"}),
            "last_name": forms.TextInput(attrs={"class": "form-control"}),
        }


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = User
        fields = (
            "email",
            "group_number",
            "first_name",
            "last_name",
        )


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = (
            "email",
            "group_number",
            "first_name",
            "last_name",
        )
