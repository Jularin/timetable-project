from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User, Homework, Lesson
from .forms import CustomUserChangeForm, CustomUserCreationForm


class CustomUserAdmin(UserAdmin):
    model = User

    add_form = CustomUserCreationForm
    form = CustomUserChangeForm

    list_display = (
        "email",
        "group_number",
        "first_name",
        "last_name",
        "is_staff",
        "is_active",
    )
    list_filter = (
        "email",
        "is_staff",
        "is_active",
    )
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "email",
                    "first_name",
                    "last_name",
                    "group_number",
                    "password",
                )
            },
        ),
        ("Permissions", {"fields": ("is_staff", "is_active")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2", "is_staff", "is_active"),
            },
        ),
    )
    search_fields = ("email",)
    ordering = ("email",)


class LessonAdmin(admin.ModelAdmin):
    model = Lesson
    list_display = (
        'homeworks',
        'name'
    )

class HomeworkAdmin(admin.ModelAdmin):
    model = Homework
    list_display = (
        'text',
        'author'
    )

admin.site.register(User, CustomUserAdmin)
admin.site.register(Lesson, LessonAdmin)
admin.site.register(Homework, HomeworkAdmin)