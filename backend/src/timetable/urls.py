from django.urls import path
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.views import get_schema_view
from rest_framework import routers
from rest_framework.permissions import IsAuthenticated

from timetable.views.auth import AuthViewSet
from timetable.views.user import UserViewSet
from timetable.views.day import DayViewset


app_name = "timetable"

router = routers.SimpleRouter()
router.register("user", UserViewSet, basename="user")
router.register("auth", AuthViewSet, basename="auth")
router.register("day", DayViewset, basename="day")

schema_view = get_schema_view(
    openapi.Info(title="Timetable API", default_version="v0.1"),
    public=True,
    generator_class=OpenAPISchemaGenerator,
    permission_classes=(IsAuthenticated,),
)


urlpatterns = [
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]

urlpatterns += router.urls
