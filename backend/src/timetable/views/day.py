from rest_framework import status, exceptions
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from rest_framework.viewsets import GenericViewSet
from rest_framework.decorators import action

from timetable.serializers.day import HomeworkSerializer, LessonSerializer
from timetable.models import Homework, Lesson


class DayViewset(GenericViewSet):
    # TODO set serializer_class and queryset

    @swagger_auto_schema(
        operation_id='get lesson', responses={200: LessonSerializer()}
    )
    @action(methods=['GET'], detail=True)
    def get_lesson(self, request, *args, **kwargs):
        try:
            lesson = Lesson.objects.get(pk=request.parser_context['kwargs']['pk'])
        except Lesson.DoesNotExist:
            return Response('Пара не найдена', status=status.HTTP_404_NOT_FOUND)
        return Response(LessonSerializer(lesson, context={'request': request}).data, status=status.HTTP_200_OK)
