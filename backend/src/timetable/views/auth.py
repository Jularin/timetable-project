from rest_framework import status, exceptions
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from rest_framework.viewsets import GenericViewSet
from rest_framework.decorators import action


from timetable.models import User
from timetable.serializers.auth import UserAuthSerializer, UserSignUpSerializer, UserAuthSignInSerializer
from timetable.services.auth import validate_sign_up_data


class AuthViewSet(GenericViewSet):
    serializer_class = UserAuthSerializer
    queryset = User.objects.all()

    @swagger_auto_schema(
        operation_id="sign up", request_body=UserSignUpSerializer, responses={200: UserAuthSerializer()}
    )
    @action(methods=["POST"], detail=False)
    def sign_up(self, request):
        email = request.data.get("email")
        password = request.data.get("password")
        group_number = request.data.get("group_number")
        first_name = request.data.get("first_name")
        last_name = request.data.get("last_name")

        if not email or not password or not first_name or not last_name:
            return Response({"error": "Forgot to enter something!"}, status=status.HTTP_400_BAD_REQUEST)

        error_messages = validate_sign_up_data(request.data)  # check if data is legit

        if error_messages:
            return Response(error_messages, status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.create_user(
            email, password, group_number, **{"first_name": first_name, "last_name": last_name}
        )
        user.save()

        return Response(self.serializer_class(user, context={"request": request}).data)

    @swagger_auto_schema(operation_id="sign in", request_body=UserAuthSignInSerializer)
    @action(methods=["POST"], detail=False)
    def sign_in(self, request):
        email = request.data.get("email")
        password = request.data.get("password")
        if not email or not password:
            return Response({"error": "Forgot to enter email or password!"}, status=status.HTTP_400_BAD_REQUEST)
        try:
            user = User.objects.get(email=email)
            if user.check_password(password):
                return Response(self.serializer_class(user, context={"request": request}).data)
            else:
                return Response({"error": "Wrong password"}, status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed("No such email")
