from django.contrib.auth.password_validation import validate_password

from timetable.models import User


def validate_sign_up_data(sign_up_data: dict) -> list:
    """Validates user's sign up data and returns list of error messages"""
    error_messages = []

    if User.objects.filter(email=sign_up_data.get("email")).exists():
        error_messages.append("Email has already been taken")

    error_messages.extend(validate_password(sign_up_data))

    return error_messages
