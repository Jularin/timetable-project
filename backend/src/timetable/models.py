from django.contrib.auth.models import AbstractUser
from django.db import models

from timetable.managers import UserManager


class User(AbstractUser):
    username = None

    group_number = models.CharField(max_length=20)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(unique=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objects = UserManager()


class Homework(models.Model):
    text = models.TextField(max_length=500)
    author = models.OneToOneField(User, on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Домашка"
        verbose_name_plural = 'Домашки'


class Lesson(models.Model):
    homeworks = models.ForeignKey(Homework, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    priority = models.SmallIntegerField(max_length=10)

    class Meta:
        verbose_name = 'Пара'
        verbose_name_plural = 'Пары'
